FROM python:3.12 as base

WORKDIR /app
COPY requirements.txt .

RUN pip3 install -r requirements.txt \
    && playwright install chromium && playwright install-deps chromium \
    && apt-get clean && apt-get autoremove --yes \
    && rm -rf /var/lib/{apt,dpkg,cache,log}/ \
    && pip3 cache purge

COPY mangaMetadata.py .

CMD ["python3", "-u", "mangaMetadata.py"]
